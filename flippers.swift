//
//  flippers.swift
//  pinBall
//
//  Created by Anuroop Desu on 4/19/16.
//  Copyright © 2016 adesu1. All rights reserved.
//

import Foundation
import SpriteKit

class Flipper: SKSpriteNode {
    
    var upperRotation: CGFloat = 0.0
    var lowerRotation: CGFloat = 0.0
    var already: Bool = false
    var initialPosition = CGPointZero
    
    func setUp() {
        
        self.physicsBody?.categoryBitMask = ObjectTypes.flipper.rawValue
        self.physicsBody?.collisionBitMask = ObjectTypes.stopper.rawValue | ObjectTypes.ball.rawValue
        self.physicsBody?.contactTestBitMask = ObjectTypes.stopper.rawValue | ObjectTypes.ball.rawValue
        self.physicsBody?.usesPreciseCollisionDetection = true
        initialPosition = self.position
        
        if self.name == "flipper_right" {
            upperRotation = -30
            lowerRotation = 30
            self.zRotation = degreesToRadians(lowerRotation)
        } else {
            upperRotation = 30
            lowerRotation = -30
            self.zRotation = degreesToRadians(lowerRotation)
        }
    }
    
    func flip() {
        if (already == false) {
            already = true
            self.physicsBody?.dynamic = true
            self.physicsBody?.allowsRotation = true
            self.physicsBody?.applyImpulse(CGVectorMake(0, 3500))
        }
    }
    
    func lower() {
        already = false
        
        self.physicsBody?.dynamic = true
        self.physicsBody?.allowsRotation = true
        self.physicsBody?.velocity = CGVectorMake(0, 0)
        self.physicsBody?.angularVelocity = 0
        
        let flip: SKAction = SKAction.rotateToAngle(degreesToRadians(lowerRotation), duration: 0.1)
        let run:SKAction = SKAction.runBlock{
            self.lockFlipperDown()
        }
        
        self.runAction(SKAction.sequence([flip, run]), withKey: "LowerthenLock")
    }
    
    func lockFlipperDown() {
        self.zRotation = degreesToRadians(lowerRotation)
        lockFlipper()
    }
    
    func lockFlipper() {
        self.physicsBody?.dynamic = false
        self.physicsBody?.allowsRotation = true
        self.physicsBody?.velocity = CGVectorMake(0,0)
        self.physicsBody?.angularVelocity = 0
    }
    
    func radiansToDegress(radians: CGFloat) -> CGFloat {
        return radians * 180 / CGFloat(M_PI)
    }
    
    func degreesToRadians(degrees: CGFloat) -> CGFloat {
        return degrees * CGFloat(M_PI) / 180
    }
    
    func update(){
        self.position = initialPosition
        
        if(self.name == "flipper_right"){
            if(self.zRotation > degreesToRadians(lowerRotation)){
                self.zRotation = degreesToRadians(lowerRotation)
            }else if(self.zRotation < degreesToRadians(lowerRotation)){
                self.physicsBody?.velocity = CGVectorMake(0, 0)
                self.physicsBody?.applyTorque(0)
                self.physicsBody?.angularVelocity = 0
                self.zRotation = degreesToRadians(upperRotation)
            }
            
        }else if(self.name == "flipper_left"){
            if(self.zRotation < degreesToRadians(lowerRotation)){
                self.zRotation = degreesToRadians(lowerRotation)
            }else if(self.zRotation > degreesToRadians(lowerRotation)){
                self.physicsBody?.velocity = CGVectorMake(0, 0)
                self.physicsBody?.applyTorque(0)
                self.physicsBody?.angularVelocity = 0
                self.zRotation = degreesToRadians(upperRotation)
            }
        }
    }
    
}