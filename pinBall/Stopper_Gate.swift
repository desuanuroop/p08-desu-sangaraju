//
//  Stopper_Gate.swift
//  pinBall
//
//  Created by Rameshkumarraju Sangaraju on 4/24/16.
//  Copyright © 2016 adesu1. All rights reserved.
//

import Foundation
import SpriteKit


class Stopper_Gate: SKSpriteNode {
    
    func setUp() {
        self.physicsBody?.categoryBitMask = ObjectTypes.gate.rawValue
        self.physicsBody?.collisionBitMask = ObjectTypes.ball.rawValue
        self.physicsBody?.contactTestBitMask = ObjectTypes.ball.rawValue
        self.physicsBody?.usesPreciseCollisionDetection = true
    }
    func collide () {
        
    }

}