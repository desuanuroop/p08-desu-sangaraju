//
//  Bumper.swift
//  pinBall
//
//  Created by Rameshkumarraju Sangaraju on 4/24/16.
//  Copyright © 2016 adesu1. All rights reserved.
//

import Foundation
import SpriteKit

class Bumper: SKSpriteNode {
    
    func setup() {
        self.physicsBody?.categoryBitMask = ObjectTypes.bumper.rawValue
        self.physicsBody?.collisionBitMask = ObjectTypes.ball.rawValue
        self.physicsBody?.contactTestBitMask = ObjectTypes.ball.rawValue
        self.physicsBody?.usesPreciseCollisionDetection = true
    }
    
    func hit(mainball: Ball) {
        if self.name == "bumper1" {
                mainball.physicsBody?.applyImpulse(CGVectorMake(50, -50))
        } else if self.name == "bumper2" {
            mainball.physicsBody?.applyImpulse(CGVectorMake(-30, 50))
        } else if self.name == "bumper3" {
            mainball.physicsBody?.applyImpulse(CGVectorMake(30, 80))
        } else if self.name == "bumper4" {
            mainball.physicsBody?.applyImpulse(CGVectorMake(15, 10))
        }
        
        /*SKAction *blink = [SKAction sequence:@[[SKAction fadeOutWithDuration:0.1],
        [SKAction fadeInWithDuration:0.1]]];
        SKAction *blinkForTime = [SKAction repeatAction:blink count:4];
        [_ship runAction:blinkForTime];*/
        
        let blink = SKAction.sequence([SKAction.fadeOutWithDuration(0.05), SKAction.fadeInWithDuration(0.05)])
        self.runAction(SKAction.sequence([blink, blink]))
    }
}
//http://stackoverflow.com/questions/23375934/backgorund-color-flash-sprite-kit