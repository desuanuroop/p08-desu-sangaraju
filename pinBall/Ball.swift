//
//  Ball.swift
//  pinBall
//
//  Created by Rameshkumarraju Sangaraju on 4/21/16.
//  Copyright © 2016 adesu1. All rights reserved.
//
import Foundation
import SpriteKit


class Ball: SKSpriteNode {
    
    var upperRotation: CGFloat = 0.0
    var lowerRotation: CGFloat = 0.0
    var already: Bool = false
    
    func setUp() {
        
        self.physicsBody?.categoryBitMask = ObjectTypes.ball.rawValue
        self.physicsBody?.collisionBitMask = ObjectTypes.flipper.rawValue | ObjectTypes.ball.rawValue
        self.physicsBody?.contactTestBitMask = ObjectTypes.flipper.rawValue | ObjectTypes.ball.rawValue
        self.physicsBody?.usesPreciseCollisionDetection = true  
    }
}