//
//  Thunder.swift
//  pinBall
//
//  Created by Rameshkumarraju Sangaraju on 4/24/16.
//  Copyright © 2016 adesu1. All rights reserved.
//

import Foundation
import SpriteKit

class Thunder: SKSpriteNode {
    
    func setup() {
        self.physicsBody?.categoryBitMask = ObjectTypes.thunder.rawValue
        self.physicsBody?.collisionBitMask = ObjectTypes.ball.rawValue
        self.physicsBody?.contactTestBitMask = ObjectTypes.ball.rawValue
        self.physicsBody?.usesPreciseCollisionDetection = true
    }
    
    func light(mainball: Ball) {
        mainball.physicsBody?.applyImpulse(CGVectorMake(0, 1000))
        }
    
}