//
//  GameScene.swift
//  pinBall
//
//  Created by Anuroop Desu on 4/19/16.
//  Copyright (c) 2016 adesu1. All rights reserved.
//

import SpriteKit
import CoreMotion
import AVFoundation

enum ObjectTypes:UInt32{
    case flipper = 1
    case ball = 3
    case extraBall = 11
    case stopper = 4
    case fireButton = 5
    case thunder = 6
    case bumper = 7
    case gate = 10
}

class GameScene: SKScene, SKPhysicsContactDelegate {
    var screenWidth:CGFloat = 0.0
    var screenHeight:CGFloat = 0.0
    var mainBall:Ball = Ball()
    var extraBall:Ball = Ball()
    var mainGate:Stopper_Gate = Stopper_Gate()
    var BallInitialPosition:CGPoint = CGPointMake(0,0)
    var extraBallInitialPosition:CGPoint = CGPointMake(0,0)
    var GatePosition: CGPoint = CGPointMake(0, 0)
    var startTime:CFTimeInterval = 0.0
    var endTime:CFTimeInterval = 0.0
    var isBallFired = false
    var motionManager: CMMotionManager!
    var background = SKSpriteNode(imageNamed: "old_paper")
    var scoreTag = SKLabelNode()
    var totalScoreLabel = SKLabelNode()
    var playLabel = SKLabelNode()
    var highScoreLabel = SKLabelNode()
    var highScoreFlag = false
    var gate_hit: Int = 0
    var lives: Int = 3
    //for various sounds
    var flipper_sound : AVAudioPlayer?
    var laser_sound : AVAudioPlayer?
    var bumper_sound : AVAudioPlayer?
    
    var totalScore:Int = 0
    var highScore: Int = 0
    var livesArray: [SKSpriteNode] = []
    var overlayArray: [SKSpriteNode] = []
    
    override func didMoveToView(view: SKView) {
        /* Setup your scene here */
        screenWidth = self.frame.width
        screenHeight = self.frame.height
        physicsWorld.contactDelegate = self;
        setUpChildren()
        //Set Motion Control
        motionManager = CMMotionManager()
        motionManager.startAccelerometerUpdates()
        
        //Set Background Image
        background.position = CGPoint(x: frame.size.width/2, y: frame.size.height/2)
        background.size = frame.size
        background.zPosition = -5
        addChild(background)
        addChild(scoreTag)
        //ScoreLabel
        totalScoreLabel.fontSize = 75
        totalScoreLabel.fontName = "Athelas Regular"
        totalScoreLabel.fontColor = UIColor .blueColor()
        totalScoreLabel.position = CGPointMake(925, 1550)
        addChild(totalScoreLabel)
        //HightScoreLabel
        highScoreLabel.fontSize = 75
        highScoreLabel.zPosition = 20
        highScoreLabel.fontName = "Chalkduster"
        highScoreLabel.position = CGPointMake(200, 1550)
        highScoreLabel.fontColor = UIColor .yellowColor()
        highScoreLabel.text = String(highScore)
        addChild(highScoreLabel)
        //playLabel
        playLabel.text = "Touch To Play"
        playLabel.position = CGPointMake(frame.size.width/2, frame.size.height/2-50)
        playLabel.fontColor = UIColor .brownColor()
        playLabel.fontSize = 100
        playLabel.fontName = "Chalkduster"
        addChild(playLabel)
        //different sounds that we use in this game
        if let flipper_sound = self.setupAudioPlayerWithFile("Pinball-buttons", type:"mp3") {
            self.flipper_sound = flipper_sound
        }
        if let laser_sound = self.setupAudioPlayerWithFile("laser_sound", type:"mp3") {
            self.laser_sound = laser_sound
        }
        if let bumper_sound = self.setupAudioPlayerWithFile("bumper", type:"mp3") {
            self.bumper_sound = bumper_sound
        }
        setLives()
    }
    
    //this function will setup Audio Player and return it
    func setupAudioPlayerWithFile(file:NSString, type:NSString) -> AVAudioPlayer?  {
        //1
        let path = NSBundle.mainBundle().pathForResource(file as String, ofType: type as String)
        let url = NSURL.fileURLWithPath(path!)
        
        //2
        var audioPlayer:AVAudioPlayer?
        
        // 3
        do {
            try audioPlayer = AVAudioPlayer(contentsOfURL: url)
        } catch {
            print("Player not available")
        }
        
        return audioPlayer
    }
    
    func setLives() {
        for i in 0..<lives {
            let life = SKSpriteNode(imageNamed: "Ball")
            let overlay = SKSpriteNode(imageNamed: "BallOverlay.png")
            life.position = CGPointMake(CGFloat(120 + (CGFloat(i) * life.size.width)), 50)
            overlay.position = CGPointMake(CGFloat(120 + (CGFloat(i) * life.size.width)), 50)
            overlay.zPosition = 20
            addChild(life)
            addChild(overlay)
            livesArray.append(life)
            overlayArray.append(overlay)
        }
}
    
    func setUpChildren() {
        for node in self.children {
            if let flipper:Flipper = node as? Flipper {
                flipper.setUp()
            }
            
            else if let flipper_stopper:flippers_stop = node as? flippers_stop {
                flipper_stopper.setUp()
            }
            
            else if let ball:Ball = node as? Ball {
                ball.setUp()
                if(ball.name == "SKSpriteNode_ball"){
                    mainBall = ball
                    BallInitialPosition.x = ball.position.x
                    BallInitialPosition.y = ball.position.y
                }
                else if ball.name == "extraBall"{
                    ball.physicsBody?.dynamic = false
                    extraBallInitialPosition.x = ball.position.x
                    extraBallInitialPosition.y = ball.position.y
                }
            }
            else if let button: FireButton = node as? FireButton {
                button.setUp()
            }
            else if let thunder: Thunder = node as? Thunder {
                    thunder.setup()
            }
            else if let bumper: Bumper = node as? Bumper {
                    bumper.setup()
            }
            else if let bumper: Bumper = node as? Bumper {
                bumper.setup()
            }
            else if let gate: Stopper_Gate = node as? Stopper_Gate {
                //gate.setUp()
                gate.hidden = true
                GatePosition = gate.position
                mainGate = gate
            }
        }
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
       /* Called when a touch begins */
        for touch in touches {
            playLabel.hidden = true
            let location = touch.locationInNode(self)
            let Touchednode = self.nodeAtPoint(location)
            
            if let _:FireButton = Touchednode as? FireButton {
                //NSLog("Fire BUtton Clicked")
                //node.fire(mainBall)
                startTime = CACurrentMediaTime()
            }
            else if(location.x < (screenWidth/2)) {
                //NSLog("Touched on Left:%f", screenWidth)
                tappedLeft()
            }else {
                //NSLog("Touched on Right:%f", screenWidth)
                tappedRight()
            }
        }
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        /* Called when a touch begins */
        for touch in touches {
            let location = touch.locationInNode(self)
            let TouchedNode = self.nodeAtPoint(location)
            if let node:FireButton = TouchedNode as? FireButton {
                endTime = CACurrentMediaTime()
                if(!isBallFired){
                    node.fire(mainBall, diff: (endTime-startTime))
                    isBallFired = true
                }
            }
            else if(location.x < (screenWidth/2)) {
                //NSLog("Touched on Left:%f", screenWidth)
                letGoLeft()
            }else {
                //NSLog("Touched on Right:%f", screenWidth)
                letGoRight()
            }
        }
    }
    
    func didBeginContact(contact: SKPhysicsContact) {
        if(contact.bodyA.categoryBitMask == ObjectTypes.ball.rawValue &&
            contact.bodyB.categoryBitMask == ObjectTypes.fireButton.rawValue){
                isBallFired = false
                mainGate.hidden = true
                mainGate.physicsBody?.categoryBitMask = 0
                
        }
        if(contact.bodyB.categoryBitMask == ObjectTypes.ball.rawValue &&
            contact.bodyA.categoryBitMask == ObjectTypes.fireButton.rawValue){
                isBallFired = false
                mainGate.hidden = true
                mainGate.physicsBody?.categoryBitMask = 0
                
        }
        if(contact.bodyA.categoryBitMask == ObjectTypes.flipper.rawValue &&
            contact.bodyB.categoryBitMask == ObjectTypes.stopper.rawValue){
                if let flipper_node:Flipper = contact.bodyA.node as? Flipper{
                    flipper_node.lockFlipper()
                }
        } else if(contact.bodyA.categoryBitMask == ObjectTypes.flipper.rawValue &&
            contact.bodyB.categoryBitMask == ObjectTypes.stopper.rawValue){
                if let flipper_node:Flipper = contact.bodyB.node as? Flipper{
                    flipper_node.lockFlipper()
                }
        }
        //Thunder
        if(contact.bodyA.categoryBitMask == ObjectTypes.ball.rawValue &&
            contact.bodyB.categoryBitMask == ObjectTypes.thunder.rawValue){
                if let thunder_node: Thunder = contact.bodyB.node as? Thunder {
                    thunder_node.light(mainBall)
                    laser_sound?.play()
                    flash()
                    scoreTags("+500", posi: thunder_node.position)
                }
        }
        if(contact.bodyB.categoryBitMask == ObjectTypes.ball.rawValue &&
            contact.bodyA.categoryBitMask == ObjectTypes.thunder.rawValue){
                if let thunder_node: Thunder = contact.bodyA.node as? Thunder {
                    thunder_node.light(mainBall)
                    laser_sound?.play()
                    flash()
                    scoreTags("+500", posi: thunder_node.position)
                }
        }
        //Bumper
        if(contact.bodyA.categoryBitMask == ObjectTypes.ball.rawValue &&
            contact.bodyB.categoryBitMask == ObjectTypes.bumper.rawValue){
                if let bumper: Bumper = contact.bodyB.node as? Bumper {
                    scoreTags("+100", posi: bumper.position)
                    bumper.hit(mainBall)
                    bumper_sound?.play()
                    totalScore += 100
                }
        }
        if(contact.bodyB.categoryBitMask == ObjectTypes.ball.rawValue &&
            contact.bodyA.categoryBitMask == ObjectTypes.bumper.rawValue){
                if let bumper: Bumper = contact.bodyA.node as? Bumper {
                    bumper.hit(mainBall)
                    scoreTags("+100", posi: bumper.position)
                    bumper_sound?.play()
                    totalScore += 100
                }
        }
        //ExtraBall
        if(contact.bodyA.categoryBitMask == ObjectTypes.ball.rawValue &&
            contact.bodyB.categoryBitMask == ObjectTypes.ball.rawValue){
                if let ball: Ball = contact.bodyB.node as? Ball {
                    if(((ball.physicsBody?.dynamic) == false)) {
                        ball.physicsBody?.dynamic = true
                    }

                }
        }
        if(contact.bodyB.categoryBitMask == ObjectTypes.ball.rawValue &&
            contact.bodyA.categoryBitMask == ObjectTypes.bumper.rawValue){
                if let bumper: Bumper = contact.bodyA.node as? Bumper {
                    bumper.hit(mainBall)
                    scoreTags("+100", posi: bumper.position)
                    bumper_sound?.play()
                    totalScore += 100
                }
        }
        
    }
    
    /*func didEndContact(contact: SKPhysicsContact) {
        
    }*/
    func flash() {
        let blink = SKAction.sequence([SKAction.fadeOutWithDuration(0.05), SKAction.fadeInWithDuration(0.05)])
        self.runAction(SKAction.sequence([blink, blink]))
        totalScore += 500
    }
    func scoreTags(score:String, var posi: CGPoint) {
        scoreTag.text = score
        scoreTag.fontSize = 80
        scoreTag.fontName = "Chalkduster"
        scoreTag.fontColor = UIColor .brownColor()
        posi.y += 90
        scoreTag.position = posi//CGPointMake(frame.size.width/2 - 50, frame.size.height/2 - 200)
    
        let blink = SKAction.sequence([SKAction.fadeInWithDuration(0.1), SKAction.fadeOutWithDuration(0.05)])
        scoreTag.runAction(SKAction.sequence([blink, blink]))
    }
    override func update(currentTime: CFTimeInterval) {
        /* Called before each frame is rendered */
        totalScoreLabel.text = String(totalScore)
        /*if highScore < totalScore {
            highScoreFlag = true
        }*/
        if highScore < totalScore {
            highScore = totalScore
            highScoreLabel.text = String(highScore)
            //Displaly HighScore Notification
        }else {
            highScoreLabel.text = String(highScore)
        }
        for node in self.children {
            if let some_flipper: Flipper = node as? Flipper{
                some_flipper.update()
            }
        }
        if extraBall.position.y < 0 && lives == 1 {
            NSLog("In extraball")
            extraBall.position = extraBallInitialPosition
            extraBall.physicsBody?.dynamic = false
        }
        
        if(mainBall.position.y < 0){
            mainBall.position = BallInitialPosition
            mainBall.physicsBody?.velocity = CGVectorMake(0, 0)
            mainGate.hidden = true
            mainGate.physicsBody?.categoryBitMask = 0
            
            let count = livesArray.count - 1
            let removeBall: SKSpriteNode = livesArray[count]
            let removeOverlay: SKSpriteNode = overlayArray[count]
            livesArray.removeAtIndex(count)
            overlayArray.removeAtIndex(count)
            removeBall .removeFromParent()
            removeOverlay .removeFromParent()
            lives -= 1
            if lives == 0 {
                //highScore = totalScore
                let alert = UIAlertView()
                alert.delegate = self
                alert.title = "Game Over"
                alert.message = "Your are Out of Lives"
                alert.addButtonWithTitle("Play Again")
                alert.addButtonWithTitle("Exit")
                alert.show()
            }
        }
        //Motion
        /*if let accelerometerData = motionManager.accelerometerData {
            //https://www.hackingwithswift.com/example-code/system/how-to-use-core-motion-to-read-accelerometer-data
            physicsWorld.gravity = CGVector(dx: accelerometerData.acceleration.y * -50, dy: accelerometerData.acceleration.x * 50)
        }*/
        
        if(mainBall.position.y > GatePosition.y) {
            mainGate.hidden = false
            mainGate.setUp()
        }
        
    }
    func alertView(View: UIAlertView!, clickedButtonAtIndex buttonIndex: Int){
        
        switch buttonIndex{
            
        case 1:
            NSLog("exit");
            exit(1)
            break;
        case 0:
            //highScoreFlag = false
            totalScore = 0
            lives = 3
            setLives()
            NSLog("replay");
            break;
        default:
            NSLog("Default");
            break;
            //Some code here..
            
        }
    }

    
    func tappedLeft() {
        for node in self.children {
            if (node.name == "flipper_left") {
                if let flipper:Flipper = node as? Flipper {
                    flipper.flip()
                    flipper_sound?.play()
                }
            }
        }
    }
    
    func tappedRight () {
        for node in self.children {
            if (node.name == "flipper_right") {
                if let flipper:Flipper = node as? Flipper {
                    flipper.flip()
                    flipper_sound?.play()
                }
            }
        }
    }
    
    func letGoLeft () {
        for node in self.children {
            if (node.name == "flipper_left") {
                if let flipper:Flipper = node as? Flipper {
                    flipper.lower()
                }
            }
        }
    }
    
    func letGoRight () {
        for node in self.children {
            if (node.name == "flipper_right") {
                if let flipper:Flipper = node as? Flipper {
                    flipper.lower()
                }
            }
        }
    }
}
