//
//  FireButton.swift
//  pinBall
//
//  Created by Anuroop Desu on 4/23/16.
//  Copyright © 2016 adesu1. All rights reserved.
//

import Foundation
import SpriteKit


class FireButton: SKSpriteNode {
    
    func setUp() {
        self.physicsBody?.categoryBitMask = ObjectTypes.fireButton.rawValue
        self.physicsBody?.collisionBitMask = ObjectTypes.ball.rawValue
        self.physicsBody?.contactTestBitMask = ObjectTypes.ball.rawValue
        self.physicsBody?.usesPreciseCollisionDetection = true
    }
    func fire (ball: Ball, diff: Double) {
        let res:CGFloat = CGFloat(0.4+diff)*CGFloat(500)
        ball.physicsBody?.applyImpulse(CGVectorMake(0, res))
    }
}